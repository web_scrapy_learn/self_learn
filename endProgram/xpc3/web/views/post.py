from django.shortcuts import render
from django.core.paginator import Paginator
from web.models import Post, Comment
from web.models import r
from django.utils.functional import cached_property


@cached_property
def count(self):
    """自定义一个count函数，替代Paginator内的count函数"""
    cached_key='post_count'
    # 先去redis内取
    rows=r.get(cached_key)
    # 如果取不到，再去数据库内查询
    if not rows:
        # 然后将查询结果存储到redis中， 这样下次就不必再查询数据库了
        # 设置1小时后过期，这样的话，1小时之后会再次查询数据库以得到最新的数量
        rows=self.object_list.count()

        r.setex(cached_key,rows,60*60*60)
    return int(rows)

# 替换原有的count函数
Paginator.count=count

def show_list(request,page=1):
    cur_page=int(page)
    ## 查询posts表，按play_counts倒序排序
    posts=Post.objects.order_by('-play_counts')
    #把视频以24页显示
    paginator=Paginator(posts,24)
    posts=paginator.page(cur_page)
    #分页逻辑、
    #要显示的页码数量
    page_num=7
    #一半的页码数量
    half_page_num=page_num//2
    first_page=1
    last_page=paginator.num_pages
    #  1,2
    if cur_page-half_page_num<1:
        #1,2,3,4,5
        #2,3,4,5,6
        # 则显示的页码从当前页开始
        display_pages=range(cur_page,cur_page+page_num)
    elif cur_page+half_page_num>last_page:
        display_pages=range(cur_page-page_num,cur_page)
    else:
        #  其他情况下，当前页左右各显示两页
        display_pages=range(cur_page-half_page_num,cur_page+half_page_num)
    display_pages=list(display_pages)
    #是否显示1下一页
    if posts.has_next():
        next_page=posts.next_page_number()
    #是否显示上一页
    if posts.has_previous():
        previous_page=posts.previous_page_number()
    # # 如果当前页码范围内不包含第一页，则把第一页添加进去
    if first_page not in display_pages:
        display_pages.insert(0,first_page)
    ## 如果当前页码范围内不包含最后一页，则把最后页添加进去
    if last_page not in display_pages:
        display_pages.append(last_page)
    return render(request,'post_list.html',locals())


def detail(request,pid):
    post=Post.objects.get(pid=pid)
    return render(request,'post.html',locals())


def comments(request):
    pid=request.GET.get('id')
    cur_page=request.GET.get('page',1)
    comments=Comment.objects.filter(pid=pid)
    paginator=Paginator(comments,10)
    comments=paginator.page(int(cur_page))
    for comment in comments:
        # reply字段存储的是commentid，如果reply不为0
        # 也就是说它是回复的另外一条评论
        if comment.reply:
            comment.reply=Comment.objects.filter(commentid=comment.reply)
    return render(request,'comments.html',locals())