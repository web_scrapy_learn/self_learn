# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymysql
from xpc.items import PostItem
class MysqlPipeline(object):
    def __init__(self):
        self.conn=None
        self.cur=None
    def open_spider(self,spider):
        self.conn=pymysql.connect(
            host='127.0.0.1',
            port=3306,
            user='root',
            password='123123',
            db='xpc_hz1801',
            charset='utf8mb4',
        )
        self.cur=self.conn.cursor()
    def process_item(self,item,spider):
        if not hasattr(item,'table_name'):
            return item
        # cols=item.keys()
        # values=list(item.values())
        cols,values=zip(*item.items())
        '''
        "insert into table1(field1,field2) values(value1,value2)"
        '''

        sql="insert into {} ({}) values ({}) on duplicate key update {}".format(
            item.table_name,
            ','.join(['%s' % col for col in cols]),
            ','.join(['%s']*len(cols)),
            ','.join('{}=%s'.format(col) for col in cols)


        )
        self.cur.execute(sql,values*2)
        self.conn.commit()
        return item


    def close_spider(self,spider):
        self.cur.close()
        self.conn.close()