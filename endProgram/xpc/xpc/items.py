# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PostItem(scrapy.Item):
    # define the fields for your item here like:
    """保存视频信息的item"""
    table_name='posts'
    p_id = scrapy.Field()
    p_img = scrapy.Field()
    p_turation = scrapy.Field()
    p_title= scrapy.Field()
    preview = scrapy.Field()
    p_video = scrapy.Field()
    p_category = scrapy.Field()
    create_at = scrapy.Field()
    play_counts = scrapy.Field()
    like_counts = scrapy.Field()


class CrItem(scrapy.Item):
    """著作权，视频和作者的对应关系"""
    # define the fields for your item here like:
    table_name='copyrights'
    pcid = scrapy.Field()
    cid = scrapy.Field()
    pid = scrapy.Field()
    roles = scrapy.Field()

class CommentItem(scrapy.Item):
    # define the fields for your item here like:

    """评论信息"""
    table_name='comments'

    comment_id = scrapy.Field()
    cid = scrapy.Field()
    pid = scrapy.Field()
    content = scrapy.Field()
    created_at = scrapy.Field()
    like_counts = scrapy.Field()
    uname = scrapy.Field()
    avatar= scrapy.Field()
    reply= scrapy.Field()

class ComposerItem(scrapy.Item):
    # define the fields for your item here like:

    """用户信息"""
    table_name = 'composers'
    cid = scrapy.Field()
    name = scrapy.Field()
    banner = scrapy.Field()
    avatar = scrapy.Field()
    verified = scrapy.Field()
    like_counts = scrapy.Field()
    fans_counts = scrapy.Field()
    follow_counts= scrapy.Field()
    location = scrapy.Field()
    career = scrapy.Field()
    intro = scrapy.Field()
hhahhaha



