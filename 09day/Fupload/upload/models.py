from django.db import models

# Create your models here.
class FileSimpleModel(models.Model):
    file_field = models.FileField(upload_to="upload/%Y/%m/%d")

