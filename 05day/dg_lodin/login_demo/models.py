from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.

class Task(models.Model):
	"""docstring for Task"""
	completed=models.BooleanField(default=False)
	title=models.CharField(max_length=100)
	def get_absolute_url(self):
		return reverse('tdlist')