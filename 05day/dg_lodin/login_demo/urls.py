from django.conf.urls import url
from login_demo import views

urlpatterns=[
	url(r'^index/',views.index,name='index'),
	url(r'^login/',views.my_login,name='login'),
	url(r'^detail/',views.detail,name='detail'),



	# --------------------------------------
	url(r'^home/$',views.home),
	url(r'^todo/$',views.todo_list,name='tdlist'),
	url(r'^todo/create/$',views.todo_create,name='create'),
	url(r'^todo/delete/(?P<pk>[\d]+)/$',views.todo_delete),
	url(r'^todo/update/(?P<pk>[\d]+)/$',views.todo_update),
	
]