from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required

from django.utils import timezone
from .models import Task

# Create your views here

def index(request):


	return HttpResponse('我是首页')


def my_login(request):
	error=None
	if request.method=='POST':
		username=request.POST.get('username')
		password=request.POST.get('password')

		user = authenticate(username=username,password=password)

		if user is not None:
			login(request,user)
			return redirect(todo_list)
		else:
			error='用户名或密码错误！'




	return render(request,'html_list/login.html',{'error':error})



@login_required
def detail(request):
	return HttpResponse('cccccccccccccccccc')

# ---------------------------------------------------------------


def home(request):
	# main_page
	
	time=timezone.localtime(timezone.now())
	return render(request, template_name='html_list/task/home.html',
                  context={'nowtime': time})

    


def todo_list(request):
	time=timezone.localtime(timezone.now())

	if request.method=="Get":
		task=Task.objects.all()
		return render(request,template_name='html_list/task/home.html',context={'todos':task,'nowtime':time})
	
		
def todo_create(request):
	if request.method=="Get":
		return render(request,template_name='html_list/task/create.html')
	title=request.POST.get('title')
	Task.objects.create(title=title)
	return redirect('tdlist')

def todo_delete(request,pk):
	try:
		task=Task.objects.get(pk=pk)
		task.delete()
	except Exception as e:
		print('删除失败！')
	return redirect('tdlist')


def todo_update(request,pk):
	task=Task.objects.get(pk=pk)
	task.completd=not task.completed
	task.save()
	return redirect('tdlist')













