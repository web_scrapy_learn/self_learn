from rest_framework import serializers
from login_demo.models import Task


class TaskSerializer(serializers.ModelSerializer):
    """
    Serialier to parse Task data
    """

    class Meta:
        model = Task
        fields = ('title', 'completed', 'id')
