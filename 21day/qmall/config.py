#开启开发者环境，打开调试模式
DEBUG=True
import os
BASE_DIR=os.path.abspath(os.path.dirname(__file__))
print(BASE_DIR)
# 定义数据库
SQLALCHEMY_DATABASE_URI ='sqlite:///'+os.path.join(BASE_DIR,'db.sqlite')
print(SQLALCHEMY_DATABASE_URI)

CSRF_ENABLED=True
CSRF_SESSION_KEY='secret'
# Secret key for signing cookies
SECRET_KEY='secretxxxxxxxxxxxxx234'