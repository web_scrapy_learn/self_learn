# 定义一个蓝图（blueprint）
from flask import Blueprint,request
# 导入密码的加密解密工
from werkzeug.security import generate_password_hash,check_password_hash
from .models import User
from flask import render_template
from .forms import UserForm,LoginForm
from app import db

module_a=Blueprint('auth',__name__,url_prefix='/auth')



@module_a.route('/crud')
def curd():
    # ————————————————————————————保存————————————————————
    password = generate_password_hash('123')
    try:
        user = User(name='f12', email='liun@qq.com', password=password)
        db.session.add(user)
        db.session.commit()
    except:
        return 'hahahhahah'

    # ________________查找____________________________________
    # 获取所有
    users=User.query.all()
    # 获取第二个
    one_user=User.query.get(2)
    user_filer=User.query.filter(User.id>3).all()
    user_filter_by=User.query.filter_by(name='f1').all()

    #______________________________修改______________
    lucy=User.query.filter_by(name='f2').first()
    if lucy:
        lucy.email='lucy@qq.com'

        db.session.add(lucy)
        db.commit()
        
    # # _________________________删除______________________
    #
    lucy = User.query.filter_by(name='f2').first()
    if lucy:


        db.session.delete(lucy)
        db.commit()
        print()

    return render_template('/module_a/signin.html',users=users,one_user=one_user,user_filer=user_filer,user_filter_by=user_filter_by)


# 127.0.0.1:5000/auth/signin/
@module_a.route('/signin',methods=['GET','POST'])
def signin():
    """
    登录
    :return:
    """
    # 把接收到的form数据绑定到UserForm的实例中
    form=LoginForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and check_password_hash(user.password,form.password.data):
            return "登录成功！"
        return "登录失败！"
    # 登录
    return render_template('/module_a/signin.html', form=form)

   # 注册`
@module_a.route('/signup',methods=['GET','POST'])
def signup():

    # form表单的使用,自动验证插件 pip install flask-wtf

    form =UserForm(request.form)
    # 验证注册form
    if form.validate_on_submit():
        name=form.name.data
        email=form.email.data
        password=generate_password_hash(form.password.data)
        print('--------------------------')
        print(password)
        user=User(name=name,email=email,password=password)
        db.session.add(user)
        db.session.commit()




        return '注册成功！'
    return render_template('/module_a/signup.html',form=form)