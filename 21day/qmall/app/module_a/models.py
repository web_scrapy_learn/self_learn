# 导入database object(db) from 主app
# app/__init__.py/db
from app import db
# 定义基本的model 为其他model提供继承

class Base(db.Model):

    __abstract__=True

    # 增加主键属性
    id=db.Column(db.Integer,primary_key=True)

class User(Base):
    __tablename__='auth_user'
    #用户名 Column 列
    name=db.Column(db.String(50),nullable=False)
    # 邮箱
    email=db.Column(db.String(128),nullable=False)
    #密码
    password=db.Column(db.String(255),nullable=False)

    # Nes instance
    def __init__(self,name,email,password):
        self.name=name
        self.email=email
        self.password=password
    def __str__(self):
        return str(self.name)
    def __repr__(self):
        return 'User %s'%(self.name)



