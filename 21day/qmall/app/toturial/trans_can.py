from flask import Flask
from flask import render_template
app=Flask(__name__)
app.debug=True

@app.route('/test_tem')
def test_tem():
    data={
        'username':"emily",
        'errors':['1','2','3']
    }
    return render_template('layout.html',**data)
    # 或者
    # return render_template('layout.html',username=data['username'],errors=data['errors'])



if __name__=='__main__':
    app.run()