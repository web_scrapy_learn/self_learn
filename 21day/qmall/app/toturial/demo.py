from flask import Flask
from flask import request
from flask import render_template
from flask import session
import pdb

app=Flask(__name__)
app.debug=True
app.secret_key='ssdfv1234567gfdsawertyubvc'

@app.route('/hello')
def test_hello():
    return 'hello world'
@app.route('/args_int/<int:x>')
def test_args(x):
    msg="x is {} ,the type is {}".format(x,type(x))
    return msg

@app.route('/args_str/<msg>')
def  test_args_str(msg):
    return 'message {} '.format(msg)

html='''
<form action='/login' method='post'>
    <input type='text' name='username'>
    <br>
    <input type='submit' value='提 交'>

</form>
'''

@app.route('/login',methods=['GET','POST'])
def login():
    if request.method=='POST':
        # pdb.set_trace()
        username=request.form.get('username','未知')
        session['username']=username
        return 'your name is {}'.format(username)
    return html

if __name__=='__main__':

    app.run()