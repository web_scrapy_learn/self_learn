"""
错误处理
"""


from flask import abort,Flask,render_template
app = Flask(__name__)
app.debug=True
@app.route('/')
def index():
    return "hello"
@app.route('/login')
def login():
    # 用户登录失败，返回401错误， 401 代表未验证的
    abort(404)
@app.errorhandler(404)
def page_not_fond(error):
    return render_template('404.html'),404
app.run(host="0.0.0.0")
