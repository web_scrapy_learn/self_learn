from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
db=SQLAlchemy()

class User(db.Model):
	__tablename__='user'
	id=db.Column(db.Integer,primary_key=True)
	username=db.Column(db.String(100),unique=True,nullable=False)
	password=db.Column(db.String(225),nullable=False)
	books=db.relationship('Book',backref='writer')
	def __init__(self,username,password):
		self.username=username
		self.password=password
class Book(db.Model):
	__tablename__="book"
	id=db.Column(db.Integer,primary_key=True)
	text=db.Column(db.String(200),nullable=False)
	time=db.Column(db.DateTime,default=datetime.utcnow)
	is_done=db.Column(db.Boolean,default=False)
	writer_id=db.Column(db.Integer,db.ForeignKey('user.id'),nullable=True)
	def to_dict(self):
		return dict(id=self.id,text=self.text,writer_id=self.writer_id,time=self.time,is_done=self.is_done)
