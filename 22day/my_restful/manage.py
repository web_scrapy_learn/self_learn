from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand

from books.models import db,Book 
from books.app import create_app

app=create_app()
migrate=Migrate(app,db)
manager=Manager(app)
manager.add_command('db',MigrateCommand)

def shell_ctx():
	return dict(
			app=app,
			db=db,
			Book=Book
		)
if __name__=='__main__':
	manager.run()