from django.db import models

# Create your models here.



class Publisher(models.Model):
    """
    出版社模型
    """
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=50)
    

class Author(models.Model):
    """
    图书作者模型
    """
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    """
    图书模型
    """
class Book(models.Model):
    title = models.CharField(max_length=100)

    # 一本书可能有多个作者，一个作者可能写过多本书，所以图书和作者是多对多关系
    authors = models.ManyToManyField(Author)

    # 一般来说，一本书只能在一个出版社出版，而一个出版社可以出版多本书，所以是1对多关系
    publisher = models.ForeignKey(Publisher)  # 出版社
