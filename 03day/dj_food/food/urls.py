from django.conf.urls import url,include
from django.contrib import admin
from food import views

urlpatterns = [
    url(r'^$', views.home,name='home'),
    url(r'^list/$',views.food_list,name='list'),
    url(r'^create/$',views.food_create),
    url(r'^delete/(?P<pk>[\d]+)/$',views.food_delete),
]

