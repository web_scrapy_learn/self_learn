from django.conf.urls import url
from user import views

urlpatterns = [
    url(r'^$', views.my_login),
    url(r'^register/$', views.my_register,name='register'),
    url(r'^code/$',views.get_code),
]
