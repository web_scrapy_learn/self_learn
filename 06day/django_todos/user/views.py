from django.shortcuts import render,redirect
from django.http import  HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.http import  HttpResponse
from  django.contrib.auth.models import User
import gvcode


from task.views import home

# Create your views here.

def get_code(request):
    base64_str,code=gvcode.base64()
    request.session['verify_code']=code
   
    return HttpResponse(base64_str)


def my_login(request):
    error = None


    if request.method == 'POST':
        
        username = request.POST.get('username')
        password = request.POST.get('password')
        user_code=request.POST.get('code')
        act_code=request.session.get('verify_code')
        user = authenticate(username=username,password=password)
        if user is not None:
           
            if user_code==act_code:
   
                login(request,user)
                return redirect(to=home)
            else:
              
                error='验证码错误！'



            
            # return redirect(to=request.GET['next'])
        else:
            error = "用户名或密码错误！"
        

    return render(request,"users/login.html",context={'error':error})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')



def my_register(request):
    error = None


    if request.method == 'POST':
       
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username,password=password) 
        # user=User.objects.filter(username=username,password=password)

       
        if user is None:
            password=make_password(password)
            user=User(username=username,password=password)
            user.save()
            # User.objects.Create(username=username,password=password)
            return render(request,"users/login.html")

        else:

            error = "用户存在！"

    # return render(request,"users/login.html",context={'error':error})      
    return render(request,"users/register.html",context={'error':error})

