from django.db import models
from django.core.urlresolvers import reverse
from  django.contrib.auth.models import User



# Create your models here.


class Task(models.Model):
    """
    Model for storing 'tasks'
    """
    # Whether this task is completed.
    completed = models.BooleanField(default=False)

    # Task title
    title = models.CharField(max_length=100)
    # user_id = models.CharField(max_length=100)
    user_id=models.ForeignKey(User)

    def get_absolute_url(self):
        return reverse('tdlist')


