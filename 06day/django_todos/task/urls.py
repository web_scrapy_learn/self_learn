from django.conf.urls import url
from task import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^todo/$', views.todo_list, name='tdlist'),
    url(r'^todo/create/$', views.todo_create, name='create'),
    url(r'^todo/delete/(?P<pk>[0-9]+)/$', views.todo_delete),
    url(r'^todo/update/(?P<pk>[0-9]+)/$', views.todo_update),
   

]
