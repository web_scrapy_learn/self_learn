from django.conf.urls import url
from testapp import views

urlpatterns=[

	url(r'^$',views.home,name='home'),
	url(r'^list/$',views.test_list,name='list'),
	url(r'^create/$',views.test_create),
	
	url(r'^delete/(?P<pk>[0-9]+)/$', views.test_delete),

]